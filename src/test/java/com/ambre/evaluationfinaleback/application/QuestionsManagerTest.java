package com.ambre.evaluationfinaleback.application;

import com.ambre.evaluationfinaleback.site.application.LanguagesManager;
import com.ambre.evaluationfinaleback.site.application.QuestionsManager;
import com.ambre.evaluationfinaleback.site.application.TagsManager;
import com.ambre.evaluationfinaleback.site.application.UsersManager;
import com.ambre.evaluationfinaleback.site.domain.model.languages.LanguageNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.tags.TagNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import com.ambre.evaluationfinaleback.site.domain.model.users.UserNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import com.ambre.evaluationfinaleback.site.domain.repository.QuestionsRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static net.bytebuddy.matcher.ElementMatchers.any;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class QuestionsManagerTest {

    @Autowired
    QuestionsManager questionsManager;
    @MockBean
    QuestionsRepository questionsRepository;
    @MockBean
    UsersManager usersManager;
    @MockBean
    TagsManager tagsManager;
    @MockBean
    LanguagesManager languagesManager;

    @Test
    void createQuestionTest() throws UserNotFoundException, TagNotFoundException, LanguageNotFoundException {

        //Création d'un utlisateur
        Users user = new Users("1", "UserName", "user@name.com", "password");
        Mockito.when(usersManager.getUserByEmail("user@name.com")).thenReturn(Optional.of(user));

        //Création d'un tag
        Tags tag1 = new Tags("ffa30121-9049-4d15-9be2-1c6c06b5d26e", "git");
        Mockito.when(tagsManager.getTagByName("git")).thenReturn(Optional.of(tag1));

        //Création d'un langue
        Languages language1 = new Languages("64d49c03-aaed-47a5-8545-fb32d11ce7a6", "Français");
        Mockito.when(languagesManager.getLanguageByName("Français")).thenReturn(Optional.of(language1));

        Mockito.when(questionsRepository.save(ArgumentMatchers.any(Questions.class))).thenReturn(new Questions("1", "test", "test", LocalDateTime.now(), language1, tag1, user));

        //test de la création d'un question avec un titre sans point d'intérogation
        Exception exception2 = assertThrows(RuntimeException.class,()-> questionsManager.createQuestion("titre correcte ", "content", LocalDateTime.now(), "git", "Français", "user@name.com" ));
        assertEquals("Le titre est incorrect", exception2.getMessage());

        //test de la création d'une question avec un titre trop long
        Exception exception3 = assertThrows(RuntimeException.class,()-> questionsManager.createQuestion("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor posuere velit. Morbi id turpis bibendum, pellentesque enim vel, mollis truc ?", "content", LocalDateTime.now(), "git", "Français", "user@name.com" ));
        assertEquals("Le titre est incorrect", exception3.getMessage());
    }
}
