package com.ambre.evaluationfinaleback.application;

import com.ambre.evaluationfinaleback.site.application.TagsManager;
import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import com.ambre.evaluationfinaleback.site.domain.repository.TagsRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TagsManagerTest {

    @Autowired
    TagsManager tagsManager;

    @MockBean
    TagsRepository tagsRepository;

    @Test
    void findTagByNameTest(){
        //On crée plusieurs tags

        Tags tag1 = new Tags("ffa30121-9049-4d15-9be2-1c6c06b5d26e", "git");
        Tags tag2 = new Tags("19648af5-e60c-44a5-8d62-b4c3f16f7ab7", "java");
        Tags tag3 = new Tags("b2234bfd-689d-4651-9134-83dd828fe637", "spring");

        List<Tags> response = new ArrayList<>();
        response.add(tag1);
        response.add(tag2);
        response.add(tag3);

        //On récupère le tableau de tag quand on appel le répo à la place de la db
        Mockito.when(tagsRepository.findByName("git")).thenReturn(tag1);

        assertEquals("git", tagsManager.getTagByName("git").get().getName());

    }
}
