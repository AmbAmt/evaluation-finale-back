package com.ambre.evaluationfinaleback.application;

import com.ambre.evaluationfinaleback.site.application.LanguagesManager;
import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import com.ambre.evaluationfinaleback.site.domain.repository.LanguagesRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class LanguagesManagerTest {

    @Autowired
    LanguagesManager languagesManager;

    @MockBean
    LanguagesRepository languagesRepository;

    @Test
    void findByLanguageNameTest(){

        Languages language1 = new Languages("64d49c03-aaed-47a5-8545-fb32d11ce7a6", "Français");
        Languages language2 = new Languages("95f96da4-42cb-4399-9689-d45b45f5b158", "Anglais");

        List<Languages> response = new ArrayList<>();
        response.add(language1);
        response.add(language2);

        Mockito.when(languagesRepository.findByName("Français")).thenReturn(language1);

        assertEquals("Français", languagesManager.getLanguageByName("Français").get().getName());

    }
}
