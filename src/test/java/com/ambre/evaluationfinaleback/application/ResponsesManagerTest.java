package com.ambre.evaluationfinaleback.application;

import com.ambre.evaluationfinaleback.site.application.*;
import com.ambre.evaluationfinaleback.site.domain.model.languages.LanguageNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.responses.Responses;
import com.ambre.evaluationfinaleback.site.domain.model.tags.TagNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import com.ambre.evaluationfinaleback.site.domain.model.users.UserNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import com.ambre.evaluationfinaleback.site.domain.repository.QuestionsRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ResponsesManagerTest {

    @Autowired
    ResponsesManager responsesManager;
    @Autowired
    QuestionsManager questionsManager;
    @MockBean
    UsersManager usersManager;
    @MockBean
    QuestionsRepository questionsRepository;
    @MockBean
    TagsManager tagsManager;
    @MockBean
    LanguagesManager languagesManager;

    @Test
    void createResponseTest() throws UserNotFoundException, TagNotFoundException, LanguageNotFoundException {
        //création d'un utilisateur
        Users user = new Users("1", "UserName", "user@name.com", "password");
        Mockito.when(usersManager.getUserByEmail("user@name.com")).thenReturn(Optional.of(user));

        //Création d'un tag
        Tags tag1 = new Tags("ffa30121-9049-4d15-9be2-1c6c06b5d26e", "git");
        Mockito.when(tagsManager.getTagByName("git")).thenReturn(Optional.of(tag1));

        //Création d'un langue
        Languages language1 = new Languages("64d49c03-aaed-47a5-8545-fb32d11ce7a6", "Français");
        Mockito.when(languagesManager.getLanguageByName("Français")).thenReturn(Optional.of(language1));

        //Création de la question
        Questions question1 = questionsManager.createQuestion("titre correcte ?", "content", LocalDateTime.now(), "git", "Français", "user@name.com");

        //création du tableau de sauvegarde
        List<Questions> questionsList = new ArrayList<>();
        questionsList.add(question1);

        Mockito.when(questionsRepository.save(ArgumentMatchers.any(Questions.class))).thenReturn(question1);


        for (Questions q : questionsManager.getAllQuestions()) {
            //test d'un contenus de 101 mots
            Exception exception1 = assertThrows(RuntimeException.class, () -> responsesManager.createResponse("Lorem ipsum dolor sit amet, " +
                            "consectetur adipiscing elit. Quisque a lectus sollicitudin risus rutrum vulputate et eget ipsum. Praesent vulputate, orci lobortis" +
                            " posuere sagittis, ex tortor rutrum felis, at molestie dui nisi in neque. Donec sit amet tempor neque. Sed ullamcorper euismod " +
                            "venenatis. Suspendisse et nibh nisi. Proin ut viverra dui. Donec non magna et augue egestas iaculis. Phasellus tristique et ipsum " +
                            "eget efficitur. Nulla orci diam, laoreet porttitor aliquet vel, ultrices eget erat. Morbi at sapien eget libero porttitor maximus " +
                            "et dignissim justo. Donec ut purus tempus, dapibus nisi id, congue mi. Cras nec feugiat diam. Pellentesque eget tellus bibendum.",
                    LocalDateTime.now(), "user@name.com", q.getId()));

            assertEquals("Le contenu de la réponse est trop long.", exception1.getMessage());

        }
    }
}

