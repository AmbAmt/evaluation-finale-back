CREATE EXTENSION IF NOT EXISTS pg_trgm;

create table if not exists users(
    id char(36) primary key,
    username text not null unique,
    email text not null unique,
    password text not null
);

create table if not exists tags(
    id char(36) primary key,
    name text not null unique
);

create table if not exists languages(
    id char(36) primary key,
    name text not null unique
);

create table  if not exists questions(
    id char(36) primary key,
    title text not null unique,
    content text not null,
    date timestamp without time zone not null default now(),
    is_problematic boolean not null default false,
    languages_id char(36) references languages(id) on delete cascade,
    tags_id char(36) references tags(id) on delete cascade,
    users_id char(36) references users(id) on delete cascade
);

--create table if not exists tags_questions(
--	tags_id char(36) references tags(id) on delete cascade,
--	questions_id char(36) references questions(id) on delete cascade,
--	primary key(tags_id, questions_id)
--);

create table if not exists responses(
    id char(36) primary key,
    content text not null,
    note int  not null default 0,
    date timestamp without time zone not null default now(),
    is_problematic boolean not null default false,
    questions_id char(36) references questions(id) on delete cascade,
    users_id char(36) references users(id) on delete cascade
);

create table if not exists roles(
    id char(36) primary key,
    name text unique not null
);

create table if not exists users_roles(
    users_id char(36) references users(id),
    roles_id char(36) references roles(id),
    primary key(users_id, roles_id)
);


