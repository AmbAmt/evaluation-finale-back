insert into languages
values('cde3c06d-4a19-4821-bcf3-2cfb64480b17', 'French')
on conflict do nothing;

insert into languages
values('83552c83-ed1c-4f1f-ae5b-84bea7e290f0', 'English')
on conflict do nothing;

insert into languages
values('33d184ce-ed12-49ea-b6be-b07a481bfb16', 'Spanish')
on conflict do nothing;

insert into languages
values('c17b34a8-8403-437c-869e-750244ade344', 'Italian')
on conflict do nothing;

insert into roles
values('d4d669ba-4b36-4e22-b9f0-363bffbc6163', 'ADMIN')
on conflict do nothing;

insert into roles
values('c8794274-6b94-4a7a-b74a-00c4403389be', 'USER')
on conflict do nothing;

insert into tags
values('a530b64c-2844-44bd-a4e7-e5b5fe153ef4', 'git')
on conflict do nothing;

insert into tags
values('d0df745a-6c88-4c26-80f8-11b3f50d2250', 'java')
on conflict do nothing;

insert into tags
values('ffa30121-9049-4d15-9be2-1c6c06b5d26e', 'sql')
on conflict do nothing;