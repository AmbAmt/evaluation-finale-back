package com.ambre.evaluationfinaleback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluationFinaleBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluationFinaleBackApplication.class, args);
	}

}
