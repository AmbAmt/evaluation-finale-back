package com.ambre.evaluationfinaleback.site.application;

import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import com.ambre.evaluationfinaleback.site.domain.repository.LanguagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LanguagesManager {

    private final LanguagesRepository languagesRepository;

    @Autowired
    public LanguagesManager(LanguagesRepository languagesRepository){
        this.languagesRepository = languagesRepository;
    }

  public Optional<Languages> getLanguageByName(String language){
        return Optional.ofNullable(languagesRepository.findByName(language));
  }

  public Iterable<Languages> getAllLanguages(){
        return  languagesRepository.findAll();
  }
}
