package com.ambre.evaluationfinaleback.site.application;

import com.ambre.evaluationfinaleback.site.domain.model.languages.LanguageNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.questions.QuestionNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.tags.TagNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.users.UserNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.repository.QuestionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class QuestionsManager {

    private final QuestionsRepository questionsRepository;
    private final UsersManager usersManager;
    private final TagsManager tagsManager; //retrouver le tag par son nim
    private final LanguagesManager languagesManager;

    @Autowired
    public QuestionsManager(QuestionsRepository questionsRepository, UsersManager usersManager, TagsManager tagsManager, LanguagesManager languagesManager){
        this.languagesManager = languagesManager;
        this.questionsRepository=  questionsRepository;
        this.tagsManager = tagsManager;
        this.usersManager = usersManager;
    }
   public Questions createQuestion(String title, String content, LocalDateTime date, String tagName, String language, String userEmail) throws UserNotFoundException, TagNotFoundException, LanguageNotFoundException {
       if(title.split(" ").length <=20 && title.charAt(title.length()-1)=='?') {
           Questions questions = new Questions(UUID.randomUUID().toString(), title, content, date, languagesManager.getLanguageByName(language).orElseThrow(() -> new LanguageNotFoundException()),
                   tagsManager.getTagByName(tagName).orElseThrow(() -> new TagNotFoundException()), usersManager.getUserByEmail(userEmail).orElseThrow(() -> new UserNotFoundException()));
           questionsRepository.save(questions);
           return questions;
       }else{
            throw new RuntimeException("Le titre est incorrect");
       }
   };

    public Optional<Questions> getQuestionById(String id){
        return  questionsRepository.findById(id);
    }

    public List<Questions> getAllQuestions(Integer pageNo, Integer pageSize, String sortBy){
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<Questions> pagedResult = (Page<Questions>) questionsRepository.findAllByOrderByDateAsc();

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Questions>();
        }
       // return questionsRepository.findAllByOrderByDateAsc();
    }

    public List<Questions> getAllQuestions(){
        return questionsRepository.findAllByOrderByDateAsc();
    }

    public Optional<Questions> getQuestion(String questionId) {
        return questionsRepository.findById(questionId);
    }

    public Optional<List<Questions>> getQuestionsByLanguages(String languageId){
        return Optional.ofNullable(questionsRepository.findByLanguageId(languageId));
    }

    public Optional<List<Questions>> getQuestionsByTitle(String title){
        return Optional.ofNullable(questionsRepository.findByTitle(title));
    }

    public Optional<List<Questions>> getQuestionsByUser(String userId){
        return Optional.ofNullable(questionsRepository.findByUserId(userId));
    }

    public Optional<List<Questions>> getAllQuestionsWithNoResponses(String noResponses){
        return Optional.ofNullable(questionsRepository.findWithNoResponses());
    }

    public Optional<Questions> isProblematic(String questionId) throws QuestionNotFoundException {
      Questions question =  questionsRepository.findById(questionId).orElseThrow(QuestionNotFoundException::new);
        if (question.isProblematic() == true) {
            question.setProblematic(false);
        } else {
            question.setProblematic(true);
        }
        this.questionsRepository.save(question);
        return Optional.of(question);
    }

    public void deleteQuestion(String questionId){
        if( questionsRepository.findById(questionId).get().isProblematic()== true){
            questionsRepository.deleteById(questionId);
        }
    }

}
