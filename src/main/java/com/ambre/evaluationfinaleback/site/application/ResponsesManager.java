package com.ambre.evaluationfinaleback.site.application;

import com.ambre.evaluationfinaleback.site.domain.model.questions.QuestionNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.responses.ResponseNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.responses.Responses;
import com.ambre.evaluationfinaleback.site.domain.model.users.UserNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import com.ambre.evaluationfinaleback.site.domain.repository.ResponsesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ResponsesManager {

    private final ResponsesRepository responsesRepository;
    private final UsersManager usersManager;
    private final QuestionsManager questionsManager;


    @Autowired
    public ResponsesManager(ResponsesRepository responsesRepository, UsersManager usersManager, QuestionsManager questionsManager){
        this.responsesRepository = responsesRepository;
        this.usersManager = usersManager;
        this.questionsManager = questionsManager;

    }

    public Responses createResponse(String content, LocalDateTime date,String userEmail, String questionId) throws UserNotFoundException, QuestionNotFoundException {
        if(content.split(" ").length<=100) {
            Responses response = new Responses(UUID.randomUUID().toString(), content, date, questionsManager.getQuestionById(questionId).orElseThrow(QuestionNotFoundException::new), usersManager.getUserByEmail(userEmail).orElseThrow(() -> new UserNotFoundException()));
            responsesRepository.save(response);
            return response;
        }else {
            throw  new RuntimeException("Le contenu de la réponse est trop long.");
        }
    }

    public Optional<List<Responses>> getResponsesByQuestionId(String questionId){
        return Optional.ofNullable(responsesRepository.findByQuestionIdOrderByNoteDesc(questionId));
    }

    public Responses voteResponse(String responseId) throws ResponseNotFoundException {
        Responses responses = responsesRepository.findById(responseId).orElseThrow(ResponseNotFoundException::new);
        responses.setNote();
        responsesRepository.save(responses);
        return responses;
    }

    public Optional<List<Responses>> getResponsesByUser(String userId){
        return Optional.ofNullable(responsesRepository.findByUserId(userId));

    }

    public Optional<Responses> isProblematic(String responseId) throws ResponseNotFoundException {
        Responses response =  responsesRepository.findById(responseId).orElseThrow(ResponseNotFoundException::new);
        if (response.isProblematic() == true) {
            response.setProblematic(false);
        } else {
            response.setProblematic(true);
        }
        this.responsesRepository.save(response);
        return Optional.of(response);
    }

    public void deleteResponse(String responseId){
        if( responsesRepository.findById(responseId).get().isProblematic()== true){
            responsesRepository.deleteById(responseId);
        }
    }
}
