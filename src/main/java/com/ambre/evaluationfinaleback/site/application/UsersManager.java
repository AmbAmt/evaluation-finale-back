package com.ambre.evaluationfinaleback.site.application;

import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import com.ambre.evaluationfinaleback.site.domain.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UsersManager {

    private final UsersRepository usersRepository;

    @Autowired
    public UsersManager(UsersRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Users createUser(String username,String email ,String password ){
        Users users = new Users(UUID.randomUUID().toString(), username, email, passwordEncoder.encode(password));
        usersRepository.save(users);
        return users;
    }

    public Optional<Users> getUser(String userId){
        Optional<Users> user= usersRepository.findById(userId);
        return user;
    }

    public Optional<Users> getUserByEmail(String email){
        return Optional.ofNullable(usersRepository.findByEmail(email));
    }
}
