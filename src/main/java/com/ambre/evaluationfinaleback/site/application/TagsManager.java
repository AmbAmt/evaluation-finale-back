package com.ambre.evaluationfinaleback.site.application;

import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import com.ambre.evaluationfinaleback.site.domain.repository.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TagsManager {

    private final TagsRepository tagsRepository;

    @Autowired
    public TagsManager(TagsRepository tagsRepository){
        this.tagsRepository = tagsRepository;
    }

    public Optional<Tags> getTagByName(String tagName){
        return Optional.ofNullable(tagsRepository.findByName(tagName));
    }

    public Iterable<Tags> getAllTags(){
        return  tagsRepository.findAll();
    }
}
