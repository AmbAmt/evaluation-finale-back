package com.ambre.evaluationfinaleback.site.web.questions;

import com.ambre.evaluationfinaleback.site.application.QuestionsManager;
import com.ambre.evaluationfinaleback.site.application.ResponsesManager;
import com.ambre.evaluationfinaleback.site.domain.model.languages.LanguageNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.questions.QuestionNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.responses.ResponseNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.responses.Responses;
import com.ambre.evaluationfinaleback.site.domain.model.tags.TagNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.users.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionsController {

    private final QuestionsManager questionsManager;
    private final ResponsesManager responsesManager;

    @Autowired
    public QuestionsController(QuestionsManager questionsManager, ResponsesManager responsesManager){
        this.questionsManager = questionsManager;
        this.responsesManager = responsesManager;
    }

    @PostMapping
    ResponseEntity<Questions> createQuestion(@RequestBody CreateQuestionDto questionDto) throws UserNotFoundException, TagNotFoundException, LanguageNotFoundException {
        return ResponseEntity.ok(questionsManager.createQuestion(questionDto.title(),  questionDto.content(), LocalDateTime.now(), questionDto.tagName(), questionDto.language(), questionDto.userEmail()));
    }

    @PostMapping("/{questionId}")
    ResponseEntity<Responses> createResponse(@PathVariable String questionId, @RequestBody CreateResponseDto responseDto) throws UserNotFoundException, QuestionNotFoundException {
        return ResponseEntity.ok(responsesManager.createResponse(responseDto.content(), LocalDateTime.now(), responseDto.userEmail(), questionId));
    }

    @GetMapping()
  /*  ResponseEntity<Iterable<Questions>> getAllQuestions(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy
    ){
        return ResponseEntity.ok(questionsManager.getAllQuestions(pageNo, pageSize, sortBy));
    }*/
    ResponseEntity<List<Questions>> getAllQuestions(@RequestParam(required = false) String languageId, @RequestParam(required = false) String title, @RequestParam(required = false) String userId, @RequestParam(required = false) String noResponses){
        if(languageId != null){
            return questionsManager.getQuestionsByLanguages(languageId).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
        }
        if(title != null){
            return questionsManager.getQuestionsByTitle(title).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
        }
        if(userId != null){
            return questionsManager.getQuestionsByUser(userId).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());

        }
        if(noResponses != null ){
            return questionsManager.getAllQuestionsWithNoResponses(noResponses).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
        }
        return ResponseEntity.ok(questionsManager.getAllQuestions());
    }

    @GetMapping("/{questionId}")
    ResponseEntity<Questions> getQuestion(@PathVariable String questionId) {
        return questionsManager.getQuestion(questionId).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }

    @PutMapping("/{questionId}")
    ResponseEntity<Questions> QuestionIsProblematic(@PathVariable String questionId) throws QuestionNotFoundException {
        return questionsManager.isProblematic(questionId).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{questionId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteQuestion(@PathVariable String questionId) {
        questionsManager.deleteQuestion(questionId);
    }

}
