package com.ambre.evaluationfinaleback.site.web.questions;

import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;

public record CreateQuestionDto(String title, String content, String tagName, String language, String userEmail) {
}
