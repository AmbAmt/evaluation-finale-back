package com.ambre.evaluationfinaleback.site.web.users;

public record CreateUsersDto(String username, String email, String password) {
}
