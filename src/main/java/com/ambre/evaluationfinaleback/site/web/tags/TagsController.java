package com.ambre.evaluationfinaleback.site.web.tags;

import com.ambre.evaluationfinaleback.site.application.LanguagesManager;
import com.ambre.evaluationfinaleback.site.application.TagsManager;
import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tags")
public class TagsController {

    private final TagsManager tagsManager;

    @Autowired
    public TagsController(TagsManager tagsManager){
        this.tagsManager = tagsManager;
    }

    @GetMapping
    ResponseEntity<Iterable<Tags>> getAllTags(){
        return ResponseEntity.ok(tagsManager.getAllTags());
    }

}
