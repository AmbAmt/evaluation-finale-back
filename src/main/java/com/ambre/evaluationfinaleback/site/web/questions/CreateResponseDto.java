package com.ambre.evaluationfinaleback.site.web.questions;

public record CreateResponseDto(String content, String userEmail) {
}
