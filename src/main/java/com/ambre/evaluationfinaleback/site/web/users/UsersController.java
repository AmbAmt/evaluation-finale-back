package com.ambre.evaluationfinaleback.site.web.users;

import com.ambre.evaluationfinaleback.site.application.UsersManager;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersManager usersManager;

    UsersController(UsersManager usersManager){
        this.usersManager = usersManager;
    }

    @PostMapping
    ResponseEntity<Users> createUser(@RequestBody CreateUsersDto usersDto){
        return ResponseEntity.ok(usersManager.createUser(usersDto.username(), usersDto.email(), usersDto.password()));
    }

    @GetMapping("/{userId}/publications")
    ResponseEntity<Users> getUser(@PathVariable String userId){
        return usersManager.getUser(userId).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }

    @GetMapping
    ResponseEntity<Users> getUserByEmail(@RequestParam(name="email") String email){
        return usersManager.getUserByEmail(email).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }


}
