package com.ambre.evaluationfinaleback.site.web.languages;

import com.ambre.evaluationfinaleback.site.application.LanguagesManager;
import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/languages")
public class LanguagesController {

    private final LanguagesManager languagesManager;

    @Autowired
    public LanguagesController(LanguagesManager languagesManager){
        this.languagesManager = languagesManager;
    }

    @GetMapping
    ResponseEntity<?> getAllLanguages(@RequestParam(required = false) String language){
       if(language!= null){
           return languagesManager.getLanguageByName(language).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
       }
        return ResponseEntity.ok(languagesManager.getAllLanguages());
    }


}
