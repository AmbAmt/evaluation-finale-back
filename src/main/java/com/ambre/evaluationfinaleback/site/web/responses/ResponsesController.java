package com.ambre.evaluationfinaleback.site.web.responses;

import com.ambre.evaluationfinaleback.site.application.ResponsesManager;
import com.ambre.evaluationfinaleback.site.domain.model.questions.QuestionNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.responses.ResponseNotFoundException;
import com.ambre.evaluationfinaleback.site.domain.model.responses.Responses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/responses")
public class ResponsesController {

    private final ResponsesManager responsesManager;

    @Autowired
    public ResponsesController(ResponsesManager responsesManager){
        this.responsesManager = responsesManager;
    }

    @GetMapping
    ResponseEntity<List<Responses>> getResponsesBy(@RequestParam(required = false) String questionId, @RequestParam(required = false) String userId){
       if(questionId != null) {
           return responsesManager.getResponsesByQuestionId(questionId).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
       }
       if(userId != null){
            return responsesManager.getResponsesByUser(userId).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
        }else {
           return ResponseEntity.notFound().build();
       }
    }

    @PutMapping("/{responseId}/vote")
    ResponseEntity<Responses> voteResponse(@PathVariable String responseId){
        try{
            return ResponseEntity.ok(responsesManager.voteResponse(responseId));
        }catch (ResponseNotFoundException e){
            return ResponseEntity.notFound().build(); 
        }
    }

    @PutMapping("/{responseId}")
    ResponseEntity<Responses> ResponseIsProblematic(@PathVariable String responseId) throws ResponseNotFoundException {
        return responsesManager.isProblematic(responseId).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{responseId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteResponse(@PathVariable String responseId) {
         responsesManager.deleteResponse(responseId);
    }




}
