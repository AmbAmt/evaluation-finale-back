package com.ambre.evaluationfinaleback.site.domain.model.questions;

import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import com.ambre.evaluationfinaleback.site.domain.model.responses.Responses;
import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="questions")
@Access(AccessType.FIELD)
public class Questions {
    @Id
    private String id;
    private String title;
    private String content;
    private LocalDateTime date;
    private boolean isProblematic;

    @ManyToOne
    @JoinColumn(name = "languages_id")
    private Languages language;

    @ManyToOne
    @JoinColumn(name = "tags_id")
    private Tags tag;

    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users user;

    protected Questions() {
        //for jpa
    }

    public Questions(String id, String title, String content, LocalDateTime date, Languages language, Tags tag, Users user) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.date = date;
        this.isProblematic = false;
        this.language = language;
        this.tag = tag;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public boolean isProblematic() {
        return isProblematic;
    }

    public void setProblematic(boolean problematic) {
        isProblematic = problematic;
    }

    public Languages getLanguage() {
        return language;
    }

    public Tags getTag() {
        return tag;
    }

    public Users getUser() {
        return user;
    }

}
