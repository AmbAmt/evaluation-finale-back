package com.ambre.evaluationfinaleback.site.domain.model.responses;

import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;

@Entity
@Table(name="responses")
@Access(AccessType.FIELD)
public class Responses {

    @Id
    private String id;
    private String content;
    private LocalDateTime date;
    private int note;
    private boolean isProblematic;

    //@JsonBackReference
    @ManyToOne
    @JoinColumn(name = "questions_id")
    private Questions question;

    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users user;

    protected Responses() {
        //for jpa
    }

    public Responses(String id, String content, LocalDateTime date, Questions question, Users user) {
        this.id = id;
        this.content = content;
        this.date = date;
        this.note = 0;
        this.isProblematic = false;
        this.question = question;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public int getVote() {
        return note;
    }

    /**
     * La note ne peut être incrémenter que d'un point par appel de la fonction
     */
    public void setNote() {
        this.note++;
    }

    public boolean isProblematic() {
        return isProblematic;
    }

    public void setProblematic(boolean problematic) {
        isProblematic = problematic;
    }

    public Questions getQuestion() {
        return question;
    }


    public Users getUser() {
        return user;
    }
}
