package com.ambre.evaluationfinaleback.site.domain.repository;

import com.ambre.evaluationfinaleback.site.domain.model.tags.Tags;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagsRepository extends CrudRepository<Tags, String> {

    public Tags findByName(String name);
}
