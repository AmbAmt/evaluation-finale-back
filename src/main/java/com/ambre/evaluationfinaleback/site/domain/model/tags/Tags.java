package com.ambre.evaluationfinaleback.site.domain.model.tags;

import javax.persistence.*;

@Entity
@Table(name = "tags")
@Access(AccessType.FIELD)
public class Tags {

    @Id
    private String id;
    private String name;

    protected Tags(){
        //for jpa
    }

    public Tags(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

