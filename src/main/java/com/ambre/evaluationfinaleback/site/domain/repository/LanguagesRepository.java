package com.ambre.evaluationfinaleback.site.domain.repository;

import com.ambre.evaluationfinaleback.site.domain.model.languages.Languages;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguagesRepository extends CrudRepository<Languages, String > {

    public Languages findByName(String name);
}
