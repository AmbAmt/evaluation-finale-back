package com.ambre.evaluationfinaleback.site.domain.repository;

import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionsRepository extends PagingAndSortingRepository<Questions, String> {

    public List<Questions> findAllByOrderByDateAsc();

    public List<Questions> findByLanguageId(String languageId);

    @Query( value = "select  * from questions where word_similarity(?, title)> 0.3 ", nativeQuery = true)
    public List<Questions> findByTitle(String title);

    public List<Questions> findByUserId(String userId);

    @Query(value = "select * from questions where questions.id NOT IN (SELECT responses.questions_id from responses)", nativeQuery = true)
    public List<Questions> findWithNoResponses();
}
