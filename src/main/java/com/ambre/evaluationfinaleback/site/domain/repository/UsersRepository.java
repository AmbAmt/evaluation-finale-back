package com.ambre.evaluationfinaleback.site.domain.repository;

import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<Users, String> {

    public Users findByEmail(String email);
    public  Users findByUsername(String username);
}
