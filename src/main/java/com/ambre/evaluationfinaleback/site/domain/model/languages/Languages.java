package com.ambre.evaluationfinaleback.site.domain.model.languages;

import javax.persistence.*;

@Entity
@Table(name= "languages")
@Access(AccessType.FIELD)
public class Languages {

    @Id
    private String id;
    private String name;

    protected Languages(){
        //for JPA
    }

    public Languages(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
