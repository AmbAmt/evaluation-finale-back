package com.ambre.evaluationfinaleback.site.domain.repository;

import com.ambre.evaluationfinaleback.site.domain.model.questions.Questions;
import com.ambre.evaluationfinaleback.site.domain.model.responses.Responses;
import com.ambre.evaluationfinaleback.site.domain.model.users.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResponsesRepository extends CrudRepository<Responses, String> {

    public List<Responses> findByQuestionIdOrderByNoteDesc(String questionId);

    public List<Responses> findByUserId(String userId);
}
